<?php
// src/AbsoluteValue/PropertyBundle/Controller/CategoryController.php
namespace AbsoluteValue\PropertyBundle\Controller;

use AbsoluteValue\PropertyBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($request->get('parent') != "") {
            $categories = $em->getRepository("PropertyBundle:Category")->findBy(array(
                'parent' => $request->get('parent')
            ));
        } else {
            $categories = $em->getRepository("PropertyBundle:Category")->findBy(array(
                'parent' => null
            ));
        }


        // Return different response depending on whether we are managing categories or requesting
        if ($request->getMethod() == "POST") {
            $jsonArray = array();

            foreach ($categories as $category) {
                $jsonArray[] = array(
                    "id" => $category->getId(),
                    "description" => $category->getDescription()
                );
            }

            return new Response(json_encode($jsonArray), 200, array("Content-Type" => "application/json"));

        } else {
            return new Response($this->renderView("PropertyBundle:Category:categories.html.twig", array(
                'categories' => $categories
            )));
        }
    }

    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $description = $request->get('description');
        $parent = $request->get('parent');

        // Get parent if applicable
        $parentCategory = $em->getRepository("PropertyBundle:Category")->findOneById($parent);

        $category = new Category();
        $em->persist($category);
        $category->setDescription($description);
        if ($parentCategory) {
            $em->persist($parentCategory);
            $parentCategory->addChild($category);
            $category->setParent($parentCategory);
        }
        $em->flush();

        if ($category->getId()) {
            $content = $this->renderView("PropertyBundle:Category:category_row.html.twig", array("category" => $category));
            return new Response(json_encode($content), 200, array("Content-Type" => "application/json"));
        } else {
            return new Response(array("message" => "failure"), 500, array("Content-Type" => "application/json"));
        }
    }

    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $description = $request->get('description');

        $category = $em->getRepository("PropertyBundle:Category")->findOneById($id);

        if ($category) {
            $em->persist($category);
            $category->setDescription($description);
            $em->flush();

            $content = $this->renderView("PropertyBundle:Category:category_row.html.twig", array(
                "category" => $category
            ));

            return new Response(json_encode($content), 200, array("Content-Type" => "application/json"));
        } else {
            return new Response(json_encode(array("message" => "fail")), 500, array("Content-Type" => "application/json"));
        }
    }

    public function typesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $parent = $request->get('parent');
        if ($parent) {
            $children = $em->getRepository("PropertyBundle:Category")->findByParent($parent);

            $types = array();
            foreach ($children as $type) {
                $types[$type->getId()] = $type->getSearchString();
            }
            return new Response(json_encode($types), 200, array("Content-Type" => "application/json"));
        } else {
            $types = array();
            $allCategories = $em->getRepository("PropertyBundle:Category")->findAll();
            foreach ($allCategories as $category) {
                $parent = $category->getParent();
                if (!is_null($parent) && !is_null($parent->getParent())) {
                    $types[$category->getId()] = $category->getSearchString();
                }
            }
            return new Response(json_encode($types), 200, array("Content-Type" => "application/json"));
        }
    }

    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get("toDelete");
        $reassignTo = $request->get("reassignTo");

        $categoryToDelete = $em->getRepository("PropertyBundle:Category")->findOneById($id);
        $categoryToReassign = $em->getRepository("PropertyBundle:Category")->findOneById($reassignTo);

        if ($categoryToDelete && $categoryToReassign) {
            $qb = $em->createQueryBuilder();
            $qb->update("PropertyBundle:Property", "p")
                ->set("p.primaryType", $qb->expr()->literal($categoryToReassign->getId()))
                ->set("p.primaryRegion", $qb->expr()->literal($categoryToReassign->getParent()->getId()))
                ->set("p.primaryClassification", $qb->expr()->literal($categoryToReassign->getParent()->getParent()->getId()))
                ->where($qb->expr()->orx(
                    $qb->expr()->eq("p.primaryType", $qb->expr()->literal($id)),
                    $qb->expr()->eq("p.primaryRegion", $qb->expr()->literal($id)),
                    $qb->expr()->eq("p.primaryClassification", $qb->expr()->literal($id))
                ));
            $qb->getQuery()->execute();

            $qb = $em->createQueryBuilder();
            $qb ->update("PropertyBundle:CategorySet", "c")
                ->set("c.type", $qb->expr()->literal($categoryToReassign->getId()))
                ->set("c.region", $qb->expr()->literal($categoryToReassign->getParent()->getId()))
                ->set("c.classification", $qb->expr()->literal($categoryToReassign->getParent()->getParent()->getId()))
                ->where($qb->expr()->orx(
                    $qb->expr()->eq("c.type", $qb->expr()->literal($id)),
                    $qb->expr()->eq("c.region", $qb->expr()->literal($id)),
                    $qb->expr()->eq("c.type", $qb->expr()->literal($id))
                ));
            $qb->getQuery()->execute();

            $em->remove($categoryToDelete);
            $em->flush();

            return new Response(json_encode($id), 200, array("Content-Type" => "application/json"));
        }
    }
}