<?php
// src/AbsoluteValue/PropertyBundle/Controller/SiteController.php
namespace AbsoluteValue\PropertyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\DateTimeType;

class SiteController extends Controller
{
    public function indexAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        // Get user Buckets
        $qb = $em   ->createQueryBuilder();
        $qb         ->select("b")
            ->from("PropertyBundle:Bucket", "b")
            ->add("where", $qb->expr()->andX(
                $qb->expr()->eq("b.user", '?1'),
                $qb->expr()->eq("b.status", '?2')
            ))
            ->orWhere("b.status = ?3");
        $qb         ->setParameter(1, $user->getId())
            ->setParameter(2, "active")
            ->setParameter(3, "shared");
        $buckets = $qb->getQuery()->getResult();

        // Get the count of activity dates that are not resolved and match the user's profile.
        // This is used to populate the calendar widget on the home page
        $date = new \DateTime();
        $fromDate = clone $date;
        $toDate = clone $date;
        $fromDate->modify("first day of this month");
        $toDate->modify("last day of this month");
        $date->setTime(0, 0);

        $qb = $em   ->createQueryBuilder();
        $qb         ->select("a.date, COUNT(a) as activity_count")
                    ->from("PropertyBundle:ActivityDate", "a")
                    ->groupBy("a.date")
                    ->orderBy("a.date", "ASC")
                    ->innerJoin("PropertyBundle:Property", "p", Join::WITH, "a.property = p.id")
                    ->where("p.status = 'active'")
                    ->andWhere($qb->expr()->orX(
                        "a.resolved = FALSE",
                        $qb->expr()->isNull("a.resolved")
                    ));

        if (!$user->getCategories()->isEmpty()) {
            $qb->leftJoin("p.categorySets", "c");

            $orx = $qb->expr()->orX();
            foreach ($user->getCategories() as $category) {
                if ($category->getParent() === null) {
                    // We're dealing with a classification
                    $orx->add($qb->expr()->eq("c.classification", $category->getId()));
                    $orx->add($qb->expr()->eq("p.primaryClassification", $category->getId()));
                } elseif ($category->getParent()->getParent() === null) {
                    // We're dealing with a region
                    $orx->add($qb->expr()->eq("c.region", $category->getId()));
                    $orx->add($qb->expr()->eq("p.primaryRegion", $category->getId()));
                } else {
                    // We're dealing with a type
                    $orx->add($qb->expr()->eq("c.type", $category->getId()));
                    $orx->add($qb->expr()->eq("p.primaryType", $category->getId()));
                }
            }
            $qb->andWhere($orx);
        }

        // We need to know which day to initialize the calendar on.
        $which = clone $qb;

        $which  ->andWhere($which->expr()->between("a.date", ":ad_from_date", ":ad_to_date"))
                ->setParameter("ad_from_date", $fromDate, \Doctrine\DBAL\Types\Type::DATETIME)
                ->setParameter("ad_to_date", $toDate, \Doctrine\DBAL\Types\Type::DATETIME)
                ->setMaxResults(1);

        $activities = $qb->getQuery()->getResult();
        $whichDay = $which->getQuery()->getResult();

        $searches = new ArrayCollection();
        // Get the property searches for this user
        $qb = $em->createQueryBuilder();
        $qb ->select("ps")
            ->from("PropertyBundle:PropertySearch", "ps")
            ->where("ps.user = :user")
            ->setParameter("user", $user)
            ->andWhere($qb->expr()->isNotNull("ps.name"));
        $searches->add(array("property" => $qb->getQuery()->getResult()));

        $qb = $em->createQueryBuilder();
        $qb ->select("ls")
            ->from("PropertyBundle:ListingSearch", "ls")
            ->where("ls.user = :user")
            ->setParameter("user", $user)
            ->andWhere($qb->expr()->isNotNull("ls.name"));
        $searches->add(array("listing" => $qb->getQuery()->getResult()));

        $qb = $em->createQueryBuilder();
        $qb ->select("rs")
            ->from("PropertyBundle:RentalSearch", "rs")
            ->where("rs.user = :user")
            ->setParameter("user", $user)
            ->andWhere($qb->expr()->isNotNull("rs.name"));
        $searches->add(array("rental" => $qb->getQuery()->getResult()));

        $qb = $em->createQueryBuilder();
        $qb ->select("ss")
            ->from("PropertyBundle:SaleSearch", "ss")
            ->where("ss.user = :user")
            ->setParameter("user", $user)
            ->andWhere($qb->expr()->isNotNull("ss.name"));
        $searches->add(array("sale" => $qb->getQuery()->getResult()));

        $qb = $em->createQueryBuilder();
        $qb ->select("o")
            ->from("PropertyBundle:OpportunitySearch", "o")
            ->where("o.user = :user")
            ->setParameter("user", $user)
            ->andWhere($qb->expr()->isNotNull("o.name"));
        $searches->add(array("opportunity" => $qb->getQuery()->getResult()));

        $today_startdatetime = new \DateTime('first day of this month');
        $today_enddatetime = new \DateTime('last day of this month');
        $today_startdatetime->modify('-13 months');
        $today_enddatetime->modify('-11 months');
        //echo $today_startdatetime->format('Y-m-d')."==".$today_enddatetime->format('Y-m-d'); die;
        $qb = $em->createQueryBuilder();
        $qb ->select("pj")
            ->from("PropertyBundle:PreviousJobs", "pj")
            ->where("pj.date >= :today_startdatetime")
            ->andWhere("pj.date <= :today_enddatetime")
            ->andWhere("pj.is_active = 1")
            ->orderBy("pj.date", "ASC")
            ->setParameter('today_startdatetime', $today_startdatetime->format('Y-m-d'))
            ->setParameter('today_enddatetime', $today_enddatetime->format('Y-m-d'));
        ;
        $previous_job = $qb->getQuery()->getResult();

        $content = $this->renderView('PropertyBundle:Site:home.html.twig', array(
            'properties' => $user->getStarredProperties(),
            'buckets' => $buckets,
            'activities' => $activities,
            'which' => $whichDay,
            'active' => 'home',
            'searches' => $searches,
            'previous_jobs'=> $previous_job,

        ));

        return new Response($content);
    }
    public function headerAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $qb = $em   ->createQueryBuilder();
        $qb = $em->createQueryBuilder();
        $qb ->select("tk")
            ->from("PropertyBundle:UserTasks", "tk")
            ->where("tk.user = :user")
            ->andWhere("tk.status = 1")
            ->orderBy("tk.deadline", "DESC")
            ->setParameter('user', $user)
        ;
        $task_count = count($qb->getQuery()->getResult());
        $tasks = $qb->getQuery()->getResult();
        $content = $this->renderView('PropertyBundle:Site:header.html.twig', array(
            'task_count'=>$task_count,
            'tasks'=>$tasks
        ));
        return new Response($content);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * Used to populate typeahead for relationships.
     */
    public function relationshipAction(Request $request)
    {
        $terms = $request->get('entity');
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select("s.id as id, s.name")
            ->from("PropertyBundle:SugarEntry", "s")
            ->andWhere("s.name LIKE ?1")
            ->setParameter(1, "%" . $terms . "%")
            ->setMaxResults(15);
        $entities = $qb->getQuery()->getResult();

        usort($entities, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });

        $entities = array("options" => array_values($entities));

        return new Response(json_encode($entities), 200, array("Content-Type" => "application/json"));
    }

    // Takes a CSV list of categories in $request->get('categories') and returns a json array representing
    // the remaining available category options
     public function categoriesAction(Request $request)
    {
        $categories = $request->get('categories');
        return new Response(json_encode($this->get('categories')->get(explode(',', $categories))), 200, array("Content-Type" => "application/json"));
    }
    // Takes a CSV list of categories in $request->get('categories') and returns a json array representing
    // the remaining available category options
    public function zonesAction(Request $request)
    {
        $categories = $request->get('zones');
        return new Response(json_encode($this->get('zones')->get(explode(',', $categories))), 200, array("Content-Type" => "application/json"));
    }

    public function hgAction()
    {
        $build = exec("hg id | awk '{print $1}'");
        return new Response($build);
    }

    public function adminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $relationshipTypes = $em->getRepository("PropertyBundle:RelationshipType")->findAll();
        $activityTypes = $em->getRepository("PropertyBundle:CustomActivityType")->findAll();
        $users = $em->getRepository("UserBundle:User")->findAll();

        $content = $this->renderView("PropertyBundle:Site:admin.html.twig", array(
            "relationshipTypes" => $relationshipTypes,
            "activityTypes" => $activityTypes,
            "users" => $users
        ));

        return new Response($content);
    }

}

?>