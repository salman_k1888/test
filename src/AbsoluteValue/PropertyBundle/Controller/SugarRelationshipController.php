<?php
// src/AbsoluteValue/PropertyBundle/Controller/SugarRelationshipController.php
namespace AbsoluteValue\PropertyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class SugarRelationshipController extends Controller
{
    public function nameAction(Request $request)
    {
        $id = $request->get('id');
        $sugarEntry = $this->getDoctrine()->getManager()->getRepository("PropertyBundle:SugarEntry")->findOneById($id);
        return new Response(json_encode($sugarEntry->getName()), 200, array('Content-Type' => 'application/json'));
    }
}