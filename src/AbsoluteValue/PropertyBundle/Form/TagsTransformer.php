<?php
// src/AbsoluteValue/PropertyBundle/Form/TagsTransformer.php
namespace AbsoluteValue\PropertyBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;

class TagsTransformer implements DataTransformerInterface
{
	private $tagManager;
	
	public function __construct($tagManager)
	{
		$this->tagManager = $tagManager;
	}
	
	public function transform($tags)
	{
		return join(', ', $tags->toArray());
	}
	
	public function reverseTransform($tags)
	{
		return $this->tagManager->loadOrCreateTags(
			$this->tagManager->splitTagNames($tags)
		);
	}
}

?>