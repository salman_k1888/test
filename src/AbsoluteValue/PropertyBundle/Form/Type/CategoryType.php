<?php
// src/AbsoluteValue/PropertyBundle/Form/Type/CategoryType.php
namespace AbsoluteValue\PropertyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class CategoryType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'div_span' => ""
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_replace($view->vars, array(
            'div_span' => $options['div_span']
        ));
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'category';
    }
}