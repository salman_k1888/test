<?php
namespace AbsoluteValue\PropertyBundle\Helpers;

class Functions
{
    private $entityManager;

    public function __construct()
    {
        $this->entityManager = $entityManager;
    }


    function stats($array, $key, $type)
    {
        $min= 0;
        $max=0;
        $average=0;
        $res = array("min"=>0,"max"=>0,"avg"=>0);

        if (!is_array($array) || count($array) == 0) return false;

        if ($key == "psm") {
            $sum_psm = 0;
            $sum_count = 0;
            if ($type == "office") {
                foreach($array as $a)
                {
                    if(is_numeric($a->getExcpValuePSM()) && $a->getExcpValuePSM() != "")
                    {
                        $min = $a->getExcpValuePSM();
                        $max = $a->getExcpValuePSM();
                        break;
                    }

                }
                foreach ($array as $a) {
                    if ($a->getExcpValuePSM() > $max || !is_numeric($max)) {
                        if(is_numeric($a->getExcpValuePSM()) && $a->getExcpValuePSM() != "")
                        $max = $a->getExcpValuePSM();

                    }
                    if ($a->getExcpValuePSM() < $min || !is_numeric($min)) {
                        if(is_numeric($a->getExcpValuePSM()) && $a->getExcpValuePSM() != "")
                        $min = $a->getExcpValuePSM();

                    }
                    $sum_psm += $a->getExcpValuePSM();
                    if($a->getExcpValuePSM() != null)
                    $sum_count++;
                }
                
                if($sum_psm != 0)
                {
                   $avg = number_format((float)($sum_psm / $sum_count),2,'.',''); 
                }else
                {
                   $avg = number_format((float)0 ,2,'.',''); 
                }
                
                
                $res = array("min" => $min, "max" => $max, "avg" => $avg);
                return $res;


            } else {
                foreach($array as $a)
                {

                    if(is_numeric($a->getOverallValuePSM()) && $a->getOverallValuePSM() != "")
                    {

                        $min = $a->getOverallValuePSM();
                        $max = $a->getOverallValuePSM();
                        break;
                    }
                }
                foreach ($array as $a) {
                    if ($a->getOverallValuePSM() > $max || !is_numeric($max)) {
                        if(is_numeric($a->getOverallValuePSM()) && $a->getOverallValuePSM() != "")
                        $max = $a->getOverallValuePSM();

                    }
                    if ($a->getOverallValuePSM() < $min || !is_numeric($min)) {

                        if(is_numeric($a->getOverallValuePSM()) && $a->getOverallValuePSM() != ""){
                            $min = $a->getOverallValuePSM(); }

                    }
                    $sum_psm += $a->getOverallValuePSM();
                    if($a->getOverallValuePSM() != null)
                    $sum_count++;
                }
                
                if($sum_psm != 0)
                {
                   $avg = number_format((float)($sum_psm / $sum_count),2,'.',''); 
                }else
                {
                   $avg = number_format((float)0 ,2,'.',''); 
                }
                

                $res = array("min" => $min, "max" => $max, "avg" => $avg );
                return $res;
            }

        }
        else if($key == "iy")
        {
            $sum_iy = 0;
            $sum_count = 0;
            foreach($array as $a)
            {
                if(is_numeric($a->getInitialYield()) && $a->getInitialYield() != "")
                {
                    $min = $a->getInitialYield();
                    $max = $a->getInitialYield();
                    break;

                }
            }

            foreach ($array as $a) {

                if ($a->getInitialYield() > $max || !is_numeric($max)) {
                    if(is_numeric($a->getInitialYield()) && $a->getInitialYield() != "")
                    $max = $a->getInitialYield();

                }
                if ($a->getInitialYield() < $min || !is_numeric($min)) {
                    if(is_numeric($a->getInitialYield()) && $a->getInitialYield() != "")
                    $min = $a->getInitialYield();

                }
                $sum_iy += $a->getInitialYield();
                if($a->getInitialYield() != null)
                $sum_count++;
            }
            
            if($sum_iy != 0)
            {
               $avg = number_format((float)($sum_iy / $sum_count),2,'.',''); 
            }else
            {
               $avg = number_format((float)0 ,2,'.',''); 
            }
                
            $res = array("min" => $min, "max" => $max, "avg" =>$avg );
            return $res;
        }
        else if ($key == "my") {
            $sum_my = 0;
            $sum_count = 0;
            $res = array("min"=>0,"max"=>0);
            foreach($array as $a)
            {
                if(is_numeric($a->getMarketYield()) && $a->getMarketYield() != "")
                {
                    $min = $a->getMarketYield();
                    $max = $a->getMarketYield();

                    break;
                }
            }
            foreach ($array as $a) {

                if ($a->getMarketYield() > $max || !is_numeric($max)) {
                    if(is_numeric($a->getMarketYield()) && $a->getMarketYield() != "")
                    $max = $a->getMarketYield();

                }
                if ($a->getMarketYield() < $min || !is_numeric($min)) {
                    if(is_numeric($a->getMarketYield()) && $a->getMarketYield() != "")
                    $min = $a->getMarketYield();

                }
                $sum_my += $a->getMarketYield();
                if($a->getMarketYield() != null)
                $sum_count++;
            }
            
            if($sum_my != 0)
            {
               $avg = number_format((float)($sum_my / $sum_count),2,'.',''); 
            }else
            {
               $avg = number_format((float)0 ,2,'.',''); 
            }
            
            $res = array("min" => $min, "max" => $max, "avg" => $avg);
            return $res;
        }
        else if ($key == "ey") {
            $sum_ey = 0;
            $sum_count = 0;
            foreach($array as $a)
            {
                if(is_numeric($a->getEquivalentYield()) && $a->getEquivalentYield() != "")
                {
                    $min = $a->getEquivalentYield();
                    $max = $a->getEquivalentYield();
                    break;

                }
            }
            foreach ($array as $a) {
                if ($a->getEquivalentYield() > $max || !is_numeric($max)) {
                    if(is_numeric($a->getEquivalentYield()) && $a->getEquivalentYield() != "")
                    $max = $a->getEquivalentYield();

                }
                if ($a->getEquivalentYield() < $min || !is_numeric($min)) {
                    if(is_numeric($a->getEquivalentYield()) && $a->getEquivalentYield() != "")
                    $min = $a->getEquivalentYield();

                }
                $sum_ey += $a->getEquivalentYield();
                if($a->getEquivalentYield() != null)
                $sum_count++;
            }
            
            if($sum_ey != 0)
            {
               $avg =  number_format((float)($sum_ey / $sum_count),2,'.',''); 
            }else
            {
               $avg = number_format((float)0 ,2,'.',''); 
            }
            
            $res = array("min" => $min, "max" => $max, "avg" => $avg);
            return $res;
        }
        else if ($key == "irr")
        {

            $sum_irr = 0;
            $sum_count = 0;
            $res = array("min"=>0,"max"=>0);

            foreach($array as $a)
            {
                if(is_numeric($a->getIrr()) && $a->getIrr() != "")
                {
                    $min = $a->getIrr();
                    $max = $a->getIrr();
                    break;

                }
            }
            foreach ($array as $a) {
                if ($a->getIrr() > $max || !is_numeric($max)) {
                    if(is_numeric($a->getIrr()) && $a->getIrr() != "")
                    $max = $a->getIrr();

                }
                if ($a->getIrr() < $min || !is_numeric($min)) {
                    if(is_numeric($a->getIrr()) && $a->getIrr() != "")
                    $min = $a->getIrr();

                }
                $sum_irr += $a->getIrr();
                if($a->getIrr() != null)
                $sum_count++;
            }
            
            if($sum_irr != 0)
            {
               $avg = number_format((float)($sum_irr / $sum_count),2,'.',''); 
            }else
            {
               $avg = number_format((float)0 ,2,'.',''); 
            }

            $res = array("min" => $min, "max" => $max, "avg" => $avg);
            return $res;

        }

    }

    function min_cols($array, $key)
    {
        if (!is_array($array) || count($array) == 0) return false;

        switch ($key) {
            case "psm";
                $max = $array[0]->getExcpValuePSM();
                $min  = $array[0]->getExcpValuePSM();
                foreach ($array as $a) {
                    if ($a->getExcpValuePSM() < $max) {
                        $max = $a->getExcpValuePSM();
                    }

                }
                return $max;
            case "iy";
                $max = $array[0]->getInitialYield();
                foreach ($array as $a) {
                    if ($a->getInitialYield() < $max) {
                        $max = $a->getInitialYield();
                    }
                }
                return $max;
            case "my";
                $max = $array[0]->getMarketYield();
                foreach ($array as $a) {
                    if ($a->getMarketYield() < $max) {
                        $max = $a->getMarketYield();
                    }
                }
                return $max;
            case "ey";
                $max = $array[0]->getEquivalentYield();
                foreach ($array as $a) {
                    if ($a->getEquivalentYield() < $max) {
                        $max = $a->getEquivalentYield();
                    }
                }
                return $max;
            default;
                $max = $array[0]->getIrr();
                foreach ($array as $a) {
                    if ($a->getIrr() < $max) {
                        $max = $a->getIrr();
                    }
                }
                return $max;
        }
    }
    public function avg_cols($array,$key)
    {

        if (!is_array($array) || count($array) == 0) return false;
        foreach ($array as $a) {

        }



    }

}
?>