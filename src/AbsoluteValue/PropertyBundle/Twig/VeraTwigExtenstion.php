<?php

namespace AbsoluteValue\PropertyBundle\Twig;

/**
 * Description of TwigExtenstion
 *
 * @author it.support
 */
class VeraTwigExtenstion extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'price' => new \Twig_Filter_Method($this, 'priceFilter'),
            'url_param' => new \Twig_Filter_Method($this, 'urlParamIsFilter'),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$' . $price;

        return $price;
    }
    
    public function urlParamIsFilter($url)
    {
        $urlParam = strpos($url, '?');
        return $urlParam;
    }

    public function getName()
    {
        return 'acme_extension';
    }
}
