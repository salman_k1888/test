<?php

// src/AbsoluteValue/PropertyBundle/Entity/Property.php

namespace AbsoluteValue\PropertyBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\Taggable\Taggable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\ExecutionContextInterface;

use Doctrine\ORM\Query\Expr\Join;

use AbsoluteValue\PropertyBundle\Entity\Sale;
use AbsoluteValue\PropertyBundle\Entity\Property;
use AbsoluteValue\PropertyBundle\Entity\Listing;
use AbsoluteValue\PropertyBundle\Entity\Lease;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\Loggable
 */
class Property implements Taggable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Gedmo\Versioned
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     * @Gedmo\Versioned
     */
    protected $streetAddress1;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     * @Gedmo\Versioned
     */
    protected $streetAddress2;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Gedmo\Versioned
     */
    protected $suburb;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Gedmo\Versioned
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     * @Gedmo\Versioned
     */
    protected $postCode;
    
    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Gedmo\Versioned
     */
    protected $latitude;
    
    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Gedmo\Versioned
     */
    protected $longitude;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected $studHeight;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Gedmo\Versioned
     */
    protected $year_built;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $land_area;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $wof;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $property_condition;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $nbs_rating;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $seismic;
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $property_frontage;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Gedmo\Versioned
     */
    protected $titleNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $unitary_plan;

    /**
     * @ORM\OneToMany(targetEntity="PreviousJobs", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $previous_jobs;

    /**
     * @ORM\OneToMany(targetEntity="PropertyGlazing", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_glazing;

    /**
     * @ORM\OneToMany(targetEntity="PropertyFlooring", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_flooring;

    /**
     * @ORM\OneToMany(targetEntity="PropertyWallLining", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_wall_lining;

    /**
     * @ORM\OneToMany(targetEntity="PropertyCladding", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_cladding;

    /**
     * @ORM\OneToMany(targetEntity="PropertyRoofing", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_roofing;

    /**
     * @ORM\OneToMany(targetEntity="PropertyHvac", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_hvac;

    /**
     * @ORM\OneToMany(targetEntity="PropertyDoors", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_doors;


    /**
     * @ORM\OneToMany(targetEntity="PropertyInfrastructure", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_infrastructure;

    /**
     * @ORM\OneToMany(targetEntity="PropertyServices", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_services;

    /**
     * @ORM\OneToMany(targetEntity="PropertyLifts", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_lifts;

    /**
     * @ORM\OneToMany(targetEntity="PropertyHeating", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_heating;

    /**
     * @ORM\OneToMany(targetEntity="PropertyAmenities", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $property_amenities;

    /**
     * Conditions under which land is held
     * -Freehold
     * -Leasehold
     * -Stratum in Freehold
     * -Stratum in Leasehold
     * -Crosslease in Freehold
     * -Crosslease in Leasehold
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $tenure;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="PropertyUnit", mappedBy="property", cascade={"persist", "remove"})
     */
    protected $propertyUnits;

    /**
     * @ORM\OneToMany(targetEntity="Lease", mappedBy="property")
     */
    protected $leases;

    /**
     * @ORM\OneToMany(targetEntity="Sale", mappedBy="property")
     */
    protected $sales;

    /**
     * @ORM\OneToMany(targetEntity="Listing", mappedBy="property")
     */
    protected $listings;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @Gedmo\Versioned
     */
    protected $primaryClassification;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @Gedmo\Versioned
     */
    protected $primaryRegion;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @Gedmo\Versioned
     */
    protected $primaryType;

    /**
     * @ORM\ManyToOne(targetEntity="Zones")
     * @Gedmo\Versioned
     */
    protected $section;

    /**
     * @ORM\ManyToOne(targetEntity="Zones")
     * @Gedmo\Versioned
     */
    protected $zone;

    /**
     * @ORM\ManyToMany(targetEntity="CategorySet", inversedBy="property", cascade={"persist"})
     */
    protected $categorySets;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $slug;

    protected $tags;

    /**
     * @ORM\ManyToMany(targetEntity="SugarRelationship", cascade={"persist", "remove"})
     */
    protected $relationships;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="property", cascade={"persist"})
     */
    protected $images;

    /**
     * @ORM\OneToMany(targetEntity="ActivityDate", mappedBy="property")
     */
    protected $opportunities;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $status;

    /**
     * @ORM\Column(type="string" ,length=10, nullable=true)
     * @Gedmo\Versioned
     */
    protected $age_refit;

    /**
     * @ORM\Column(type="string", length=50, nullable=true )
     * @Gedmo\Versioned
     */
    protected $foundation;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Gedmo\Versioned
     */
    protected $frame;

    public function getTags()
    {
        $this->tags = $this->tags ? : new ArrayCollection();
        return $this->tags;
    }

    /**
     * This function transforms the csv tag list into the required ArrayCollection
     */
    public function setTags($tags)
    {
        $this->tags->clear();
        foreach ($tags as $tag) {
            $this->tags->add($tag);
        }
    }

    public function getTaggableType()
    {
        return 'property';
    }

    public function getTotalFloorArea()
    {
        $sum = 0;

        foreach ($this->getPropertyUnits() as $unit) {
            if ($unit->getUnit() == "psm") {
                $sum += $unit->getQuantity();
            }
        }

        return $sum;
    }

    public function getTaggableId()
    {
        return $this->getId();
    }

    public function __construct()
    {
        $this->propertyUnits = new ArrayCollection();
        $this->relationships = new ArrayCollection();
        $this->listings = new ArrayCollection();
        $this->previous_jobs = new ArrayCollection();
        $this->property_glazing = new ArrayCollection();
        $this->property_flooring = new ArrayCollection();
        $this->property_wall_lining = new ArrayCollection();
        $this->property_cladding = new ArrayCollection();
        $this->property_roofing = new ArrayCollection();
        $this->property_hvac = new ArrayCollection();
        $this->property_doors = new ArrayCollection();
        $this->property_infrastructure = new ArrayCollection();
        $this->property_services = new ArrayCollection();
        $this->property_heating = new ArrayCollection();
        $this->property_amenities = new ArrayCollection();
        $this->property_lifts = new ArrayCollection();
        $this->zones = new ArrayCollection();
        $this->status = "active";
    }

    public function getMainTenant()
    {
        $mainTenants = array();
        foreach($this->getRelationships() as $sugarRelationship) {
            if ($sugarRelationship->getType()->getDescription() == "Main Tenant") {
                $mainTenants[] = $sugarRelationship->getTarget()->getName();
            }
        }
        return implode(", ", $mainTenants); 
    }

    /**
     * Used to return an address that is encoded for google maps
     */
    public function getMapAddress()
    {
        $map = urlencode($this->getStreetAddress1());
        $map .= "," . urlencode($this->getSuburb());
        $map .= "," . urlencode($this->getCity());
        $map .= "," . urlencode($this->getPostCode());
        $map .= "," . urlencode("New Zealand");
        return $map;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set streetAddress1
     *
     * @param string $streetAddress1
     * @return Property
     */
    public function setStreetAddress1($streetAddress1)
    {
        $this->streetAddress1 = $streetAddress1;

        return $this;
    }

    /**
     * Get streetAddress1
     *
     * @return string
     */
    public function getStreetAddress1()
    {
        return $this->streetAddress1;
    }

    /**
     * Set streetAddress2
     *
     * @param string $streetAddress2
     * @return Property
     */
    public function setStreetAddress2($streetAddress2)
    {
        $this->streetAddress2 = $streetAddress2;

        return $this;
    }

    /**
     * Get streetAddress2
     *
     * @return string
     */
    public function getStreetAddress2()
    {
        return $this->streetAddress2;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     * @return Property
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Property
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return Property
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }
    
    /**
     * Get Latitude
     *
     * @return string
     */
    function getLatitude() {
        return $this->latitude;
    }
    /**
     * Get Longitude
     *
     * @return string
     */
    function getLongitude() {
        return $this->longitude;
    }
    
    /**
     * Set Latitude
     *
     * @return string
     */
    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }
    
    /**
     * Set Latitude
     *
     * @return string
     */
    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
    
    
    /**
     * Set titleNumber
     *
     * @param string $titleNumber
     * @return Property
     */
    public function setTitleNumber($titleNumber)
    {
        $this->titleNumber = $titleNumber;

        return $this;
    }

    /**
     * Get titleNumber
     *
     * @return string
     */
    public function getTitleNumber()
    {
        return $this->titleNumber;
    }

    /**
     * Add images
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Image $images
     * @return Property
     */
    public function addImage(\AbsoluteValue\PropertyBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Image $images
     */
    public function removeImage(\AbsoluteValue\PropertyBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Property
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
