<?php
// src/AbsoluteValue/PropertyBundle/Entity/Category.php

namespace AbsoluteValue\PropertyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"remove"})
     */
    protected $children;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $fixed;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fixed = false;
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getDescription();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set parent
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\AbsoluteValue\PropertyBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AbsoluteValue\PropertyBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Category $children
     * @return Category
     */
    public function addChild(\AbsoluteValue\PropertyBundle\Entity\Category $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Category $children
     */
    public function removeChild(\AbsoluteValue\PropertyBundle\Entity\Category $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set fixed
     *
     * @param boolean $fixed
     * @return Category
     */
    public function setFixed($fixed)
    {
        $this->fixed = $fixed;

        return $this;
    }

    /**
     * Get fixed
     *
     * @return boolean 
     */
    public function getFixed()
    {
        return $this->fixed;
    }

    /**
     * This method is used to generate a string for this category to be used in the property search
     */
    public function getSearchString()
    {
        $searchString = $this->getDescription();

        if ($this->getParent()) {
            return $this->getParent()->getSearchString() . " / " . $searchString;
        } else {
            return $searchString;
        }
    }
}
