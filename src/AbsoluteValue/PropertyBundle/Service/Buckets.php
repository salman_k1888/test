<?php
// src/AbsoluteValue/PropertyBundle/Service/Buckets.php
namespace AbsoluteValue\PropertyBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

class Buckets
{
    protected $em;
    protected $securityContext;

    public function __construct(EntityManager $em, SecurityContext $securityContext)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
    }

    public function get()
    {
        $loggedInUser = $this->securityContext->getToken()->getUser();
        $qb = $this->em->createQueryBuilder();
        $qb->select("b")
            ->from("PropertyBundle:Bucket", "b")
            ->add("where", $qb->expr()->andX(
                $qb->expr()->eq("b.user", '?1'),
                $qb->expr()->eq("b.status", '?2')
            ))
            ->orWhere("b.status = ?3");
        $qb->setParameter(1, $loggedInUser)
            ->setParameter(2, "active")
            ->setParameter(3, "shared");
        return $qb->getQuery()->getResult();
    }
    
}