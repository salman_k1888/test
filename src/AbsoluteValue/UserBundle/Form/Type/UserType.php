<?php // src/AbsoluteValue/UserBundle/Form/Type/UserType.php

namespace AbsoluteValue\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('username', 'text')
                ->add('email', 'email')
                ->add('password', 'password')
                ->add('save', 'submit');
        
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AbsoluteValue\UserBundle\Entity\User'
        ));
    }
    
    public function getName() {
        return 'user';
    }
}