<?php
// src/AbsoluteValue/UserBundle/Form/Type/ProfileType.php

namespace AbsoluteValue\UserBundle\Form\Type;

use AbsoluteValue\PropertyBundle\Form\Type\EntityHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email')
            ->add('password', 'password', array(
                'required' => false,
                'mapped' => false
            ))
            ->add('categories', 'collection', array(
                'type' => 'entity_hidden',
                'options' => array(
                    'class' => 'PropertyBundle:Category'
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AbsoluteValue\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return "profile";
    }
}