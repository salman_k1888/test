<?php
// src/AbsoluteValue/UserBundle/Controller/OptionController.php
namespace AbsoluteValue\UserBundle;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

use AbsoluteValue\UserBundle\Entity\Option;

class UserOption
{
    protected $em;
    protected $securityContext;

    public function __construct(EntityManager $em, SecurityContext $security)
    {
        $this->em = $em;
        $this->securityContext = $security;
    }

    public function get($name)
    {
        $loggedInUser = $this->securityContext->getToken()->getUser();
        $option = $this->em->getRepository("UserBundle:Option")->findOneBy(array(
            'user' => $loggedInUser,
            'name' => $name
        ));

        if ($option) {
            return $option->getValue();
        } else {
            return null;
        }
    }

    public function set($names, $value = null)
    {
        $loggedInUser = $this->securityContext->getToken()->getUser();

        if (!is_array($names) && isset($names))
            $names = array($names => $value);

        foreach ($names as $key => $value) {
            if (!is_null($value)) {
                if (!$this->has($key)) {
                    $option = new Option();

                    $this->em->persist($option);
                    $option->setName($key);
                    $option->setValue($value);
                    $option->setUser($loggedInUser);
                    $this->em->flush();

                } else {

                    $option = $this->em->getRepository("UserBundle:Option")->findOneBy(array(
                        'user' => $loggedInUser,
                        'name' => $key
                    ));

                    $this->em->persist($option);
                    $option->setValue($value);
                    $this->em->flush();
                }
            }
        }
    }

    public function has($name)
    {
        $exists = $this->get($name);
        if (is_null($exists)) {
            return false;
        } else {
            return true;
        }
    }

    public function remove($names)
    {
        if (!is_array($names))
            $names = array($names);

        $loggedInUser = $this->securityContext->getToken()->getUser();
        foreach ($names as $name) {
            $option = $this->em->getRepository("UserBundle:Option")->findOneBy(array(
                'user' => $loggedInUser,
                'name' => $name
            ));
            if ($option) {
                $this->em->remove($option);
                $this->em->flush();
            }
        }
    }

    public function anytrue($names)
    {
        if (!is_array($names))
            $names = array($names);

        foreach ($names as $name) {
            if ($this->get($name))
                return true;
        }
    }

}

?>