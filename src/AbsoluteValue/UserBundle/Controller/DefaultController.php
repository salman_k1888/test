<?php
// src/AbsoluteValue/UserBundle/Controller/DefaultController.php
namespace AbsoluteValue\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use AbsoluteValue\UserBundle\Entity\User;
use AbsoluteValue\UserBundle\Form\Type\ProfileType;
use AbsoluteValue\UserBundle\Form\Type\UserType;

class DefaultController extends Controller
{
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        $content = $this->renderView('::login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error' => $error
            )
        );
        return new Response($content);
    }

    public function passwordResetRequestAction(Request $request)
    {
        $email = $request->get("email_address");
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UserBundle:User")->findOneByEmail($email);

        if ($user) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $token = $encoder->encodePassword($this->randomPassword(), $user->getSalt());
            $user->setPasswordResetToken($token);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject("VERA Password Reset Instructions")
                ->setFrom("admin@absolutevalue.co.nz")
                ->setTo($user->getEmail())
                ->setBody($this->renderView("::reset_password_email.html.twig", array("token" => $token)), 'text/html');

            $this->get('mailer')->send($message);

            return new Response(json_encode("success"), 200, array("Content-Type" => "application/json"));
        } else {
            return new Response(json_encode("failure"), 500, array("Content-Type" => "application/json"));
        }
    }

    public function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function passwordResetAction($token, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UserBundle:User")->findOneByPasswordResetToken($token);
        $newPassword = $request->get("_password");

        if (!$user) {
            return $this->redirect($this->generateUrl("home"));
        }

        if ($newPassword) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($newPassword, $user->getSalt());

            $user->setPassword($password);
            $user->setPasswordResetToken(null);
            $em->flush();

            return $this->redirect($this->generateUrl("home"));
        } else {
            $content = $this->renderView("::reset.html.twig");
        }

        return new Response($content);
    }

    public function deniedAction()
    {
        return new Response($this->renderView("UserBundle:Default:denied.html.twig"));
    }

    /**
     * @Route("/", name="user_index")
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->findByIsActive(true);
        $content = $this->renderView('UserBundle:Default:users.html.twig', array('users' => $users));
        return new Response($content);
    }

    /**
     * @Route("/{username}/delete", name="user_delete")
     * @ParamConverter("user")
     */
    public function deleteAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $loggedInUser = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() == $loggedInUser->getId()) {
            $this->get('session')->getFlashBag()->add('warning', 'You cannot delete your own account.');
            return $this->redirect($this->generateUrl('users'));
        }

        //TODO prevent user from deleting higher order user

        $em->remove($user);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', "User with username: " . $user->getUserName() . " deleted successfully.");
        return $this->redirect($this->generateUrl('users'));
    }

    /**
     * @Route("/new", name="user_new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $form = $this->createForm(new UserType(), $user);
        $role = $em->getRepository('UserBundle:Role')->findOneByRole('ROLE_USER');

        if (!$role) {
            $role = new Role();
            $em->persist($role);
            $role->setName("ROLE_USER");
            $role->setRole("ROLE_USER");
            $em->flush();
        }

        //Handle the request
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($user);

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
            $user->addRole($role);

            $em->flush();

            // Set user defaults
            $uo = $this->get('user.option');
            $uo->set(array(

            ));


            $this->get('session')->getFlashBag()->add('success', 'User with username: ' . $user->getUsername() . ' added successfully.');
            return $this->redirect($this->generateUrl('users'));
        } else {
            $content = $this->renderView('UserBundle:Default:update.html.twig', array(
                'form' => $form->createView()
            ));
            return new Response($content);
        }
    }

    /**
     * @Route("/{username}/update", name="user_update")
     * @ParamConverter("user")
     */
    public function updateAction(User $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'User with username: ' . $user->getUsername() . ' updated successfully.');
            return $this->redirect($this->generateUrl('users'));
        } else {
            $content = $this->renderView('UserBundle:Default:update.html.twig', array(
                'form' => $form->createView()
            ));

            return new Response($content);
        }
    }

    public function updatePerPageAction(Request $request)
    {
        $per_page = $request->get('per_page');
        $this->get('user.option')->set('results_per_page', $per_page);

        $content = json_encode(array('message' => 'Success'));
        return new Response($content, 200, array('Content-Type' => 'application/json'));
    }

    /**
     * @Route("/profile", name="user_profile")
     */
    public function profileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException("The user could not be loaded from the security context.");
        }

        $form = $this->createForm(new ProfileType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $password = $form["password"]->getData();

            if (!empty($password)) {
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
            }

            $em->flush();

            $content = $this->renderView("UserBundle:Default:profile.html.twig", array(
                "form" => $form->createView(),
                "submitted" => true,
            ));


            return new Response(json_encode($content), 200, array('Content-Type' => 'application/json'));
        } else {
            $content = $this->renderView("UserBundle:Default:profile.html.twig", array(
                "form" => $form->createView()
            ));

            return new Response($content);
        }
    }

    /**
     * @Route("/categories", name="user_categories")
     *
     * Assists with "watched categories" to prepare UI and form elements when selecting CategorySets
     */
    public function selectAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        $category = $em->getRepository("PropertyBundle:Category")->findOneById($id);

        if ($category) {
            // Get children Ids
            // We start by getting the children of this category. Since on the input side, categories are limited to
            // three levels deep, we can just do a 2 level nested loop. If we were to ever remove this 3 level
            // constraint, we would need to use recursion for operations like this.
            $children = $category->getChildren();
            $childrenIds = array();

            foreach ($children as $cat) {
                $childrenIds[] = $cat->getId();
                foreach ($cat->getChildren() as $child) {
                    $childrenIds[] = $child->getId();
                }
            }


            $view = $this->renderView("UserBundle:Default:category_li.html.twig", array(
                'category' => $category,
                'childrenIds' => join(",", $childrenIds)
            ));

            return new Response(json_encode($view), 200, array('Content-Type' => 'application/json'));
        } else {
            throw $this->createNotFoundException("Could not find the category");
        }
    }
}
