<?php

// src/AbsoluteValue/UserBundle/Entity/User.php

namespace AbsoluteValue\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * AbsoluteValue\UserBundle\Entity\User
 *
 * @ORM\Entity(repositoryClass="AbsoluteValue\UserBundle\Entity\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $salt;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $resetPasswordOnNextLogin;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    protected $passwordResetToken;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    protected $roles;

    /**
     * @ORM\ManyToMany(targetEntity="\AbsoluteValue\PropertyBundle\Entity\Property")
     */
    protected $starredProperties;

    /**
     * @ORM\ManyToMany(targetEntity="\AbsoluteValue\PropertyBundle\Entity\Category", cascade={"persist"})
     */
    protected $categories;

    public function __construct()
    {
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
        $this->roles = new ArrayCollection();
        $this->starredCompanies = new ArrayCollection();
        $this->starredContacts = new ArrayCollection();
        $this->starredProperties = new ArrayCollection();
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles->toArray();
    }

    public function eraseCredentials()
    {

    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->id,
            ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add roles
     *
     * @param \AbsoluteValue\UserBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\AbsoluteValue\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AbsoluteValue\UserBundle\Entity\Role $roles
     */
    public function removeRole(\AbsoluteValue\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Add starredProperties
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Property $starredProperties
     * @return User
     */
    public function addStarredProperty(\AbsoluteValue\PropertyBundle\Entity\Property $starredProperties)
    {
        $this->starredProperties[] = $starredProperties;

        return $this;
    }

    /**
     * Remove starredProperties
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Property $starredProperties
     */
    public function removeStarredProperty(\AbsoluteValue\PropertyBundle\Entity\Property $starredProperties)
    {
        $this->starredProperties->removeElement($starredProperties);
    }

    /**
     * Get starredProperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStarredProperties()
    {
        return $this->starredProperties;
    }

    /**
     * Add categorySets
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\CategorySet $categorySets
     * @return User
     */
    public function addCategorySet(\AbsoluteValue\PropertyBundle\Entity\CategorySet $categorySets)
    {
        $this->categorySets[] = $categorySets;

        return $this;
    }

    /**
     * Add categories
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Category $categories
     * @return User
     */
    public function addCategory(\AbsoluteValue\PropertyBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \AbsoluteValue\PropertyBundle\Entity\Category $categories
     */
    public function removeCategory(\AbsoluteValue\PropertyBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * Set resetPasswordOnNextLogin
     *
     * @param boolean $resetPasswordOnNextLogin
     * @return User
     */
    public function setResetPasswordOnNextLogin($resetPasswordOnNextLogin)
    {
        $this->resetPasswordOnNextLogin = $resetPasswordOnNextLogin;

        return $this;
    }

    /**
     * Get resetPasswordOnNextLogin
     *
     * @return boolean 
     */
    public function getResetPasswordOnNextLogin()
    {
        return $this->resetPasswordOnNextLogin;
    }

    /**
     * Set passwordResetToken
     *
     * @param string $passwordResetToken
     * @return User
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * Get passwordResetToken
     *
     * @return string 
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }
    public function __toString()
    {
        return $this->getEmail();
    }
}
