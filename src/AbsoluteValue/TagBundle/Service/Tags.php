<?php
// src/AbsoluteValue/TagBundle/Services/Tags.php

namespace AbsoluteValue\TagBundle\Service;

use Doctrine\ORM\EntityManager;

class Tags
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function get($type = null)
    {
        $tagRepository = $this->em->getRepository("TagBundle:Tag");

        if (is_null($type)) {
            return $tagRepository->findAll();
        } else {
            return $tagRepository->getTagsQueryBuilder($type)->getQuery()->getResult();
        }
    }
}