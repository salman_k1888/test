<?php

namespace AbsoluteValue\ServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AbsoluteValue\PropertyBundle\Entity\Proeprty;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer;
class DefaultController extends FOSRestController
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('match'));
        $encoder = new JsonEncoder();
        $serializer = new Serializer(array($normalizer), array($encoder));

        $id = $request->request->get('id');
        try {
            $qb = $em->createQueryBuilder();
            $property = $em->getRepository("PropertyBundle:Property")->findOneById($id);
            return array(
                'response' => $property,
                'error'=>false
            );
        }
        catch(\Exception $e){
            $response = new Response(
                json_encode(array('response' => $e->getMessage())), 200, array('Content-Type' => 'application/json')
            );
            return $response;
        }
    }
    public function BucketListingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('match')); //Replace match with the parent attribute
        $encoder = new JsonEncoder();
        $serializer = new Serializer(array($normalizer), array($encoder));
        $user = $request->request->get('user');

        try {
            $qb = $em->createQueryBuilder();
            $bucket = $em->getRepository("PropertyBundle:Bucket")->findAll();
            $response_array= array();
            foreach($bucket as   $b)
            {
                $response_array[$b->getId()]=$b->getName();
            }

            return array(
                'response' => $response_array,
                'error'=>false
            );
            return array(
                'response' => $response_array,
                'error'=>false
            );
        }
        catch(\Exception $e){
            $response = new Response(
                json_encode(array('response' => $e->getMessage())), 200, array('Content-Type' => 'application/json')
            );
            return $response;
        }
    }
}
