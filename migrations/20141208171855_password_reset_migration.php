<?php

use Phinx\Migration\AbstractMigration;

class PasswordResetMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table("User");
        $table
            ->addColumn('resetPasswordOnNextLogin', 'boolean', array('default' => null, "null" => true))
            ->addColumn('passwordResetToken', 'string', array('limit' => 40, 'null' => true))
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table("User");
        $table
            ->removeColumn('resetPasswordOnNextLogin')
            ->removeColumn('passwordResetToken')
            ->save();
    }
}